import React, {Component} from 'react';
import '../css/Main.css';
import {Routes, Route} from 'react-router-dom';
import Weather from './Weather'
import Login from './Login'

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isUserLoggedIn : false
        }
        this.setIsUserLoggedIn = this.setIsUserLoggedIn.bind(this);
    }

    setIsUserLoggedIn(isLoggedIn) {
        this.setState({
            isUserLoggedIn: isLoggedIn
        });
    }

    render() {
        return (
            <Routes>
                <Route path='login' element={<Login isUserLoggedIn={this.state.isUserLoggedIn} setIsUserLoggedIn={this.setIsUserLoggedIn}/>}/>
                <Route path='dashboard' element={<Weather isUserLoggedIn={this.state.isUserLoggedIn} setIsUserLoggedIn={this.setIsUserLoggedIn}/>}/>
                <Route path='/' element={<Login isUserLoggedIn={this.state.isUserLoggedIn} setIsUserLoggedIn={this.setIsUserLoggedIn}/>}/>
            </Routes>
        );
    }
}

export default Main;
