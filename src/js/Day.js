import '../css/Day.css';
import React from "react";
import * as dayjs from "dayjs";

function getDate(date) {
    const currentDate = dayjs();
    const nextDay = currentDate.add(1,'day');
    const date0 = dayjs(date);
    if (date0.isSame(currentDate, 'day')) return 'Today';
    if (date0.isSame(nextDay, 'day')) return 'Tomorrow';
    return (date0.format('ddd D MMM'));
}

function Day(props) {
    const windDirection = props.windDirection;
    const windDirectionStyle = {
        transform: `rotate(${windDirection}deg)`
    };
    return (
        <div className="Day">
            <h3 className="Day-header">{getDate(props.date)}</h3>
            <img src={props.weatherPicture} className="Day-weather-picture" alt="Weather state"/>
            <p className="Day-weather-state">{props.weatherState}</p>
            <p className="Day-max-temperature">Max: {props.maxTemp}&deg;C</p>
            <p className="Day-min-temperature">Min: {props.minTemp}&deg;C</p>
            <span className="Day-wind-direction" style={windDirectionStyle}/>
            <span className="Day-wind-speed"> {props.windSpeed}mph</span>
            <p className="Day-humidity">Humidity: {props.humidity}%</p>
            <p className="Day-visibility">Visibility: {props.visibility} miles</p>
            <p className="Day-pressure">Pressure: {props.pressure}mb</p>
            <p className="Day-confidence">Confidence: {props.confidence}%</p>
        </div>
    );
}

export default Day;
