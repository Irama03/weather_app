import '../css/Login.css';
import React, {Component} from "react";
import {Navigate} from "react-router-dom";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            correctUsername: 'ira3',
            correctPassword: 'ira3',
        }
        this.processSubmit = this.processSubmit.bind(this);
    }

    processSubmit(event) {
        event.preventDefault();
        const usN = document.getElementById('Login-username').value;
        const pas = document.getElementById('Login-password').value;
        if (usN !== this.state.correctUsername || pas !== this.state.correctPassword) {
            alert('Incorrect username or password!');
        }
        else this.props.setIsUserLoggedIn(true);
    }

    render() {
        return (
            <div>
                {this.props.isUserLoggedIn ? <Navigate to="/dashboard" replace={true}/> : null}
                <div className='Login-background'>
                    <form className="Login-form" onSubmit={this.processSubmit}>
                        <h3 className="Login-header">Log to Weather App</h3>
                        <p><label htmlFor="username">Username:</label></p>
                        <p><input type="text" name="username" className="Login-username" id="Login-username" placeholder="Enter username" required={true}/></p>
                        <p><label htmlFor="password">Password:</label></p>
                        <p><input type="password" name="password" className="Login-password" id="Login-password" placeholder="Enter password" required={true}/></p>
                        <button type="submit" className="Login-button" id="Login-button">Login</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Login;


